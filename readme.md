# Kentico Cloud Adapter for Sitemap Generator
Adapter for site-generator to connect to Kentico Cloud

## required environment variables

| Env      | description |
|----------|----------|
| LANGUAGE | language required by kentico cloud |
|KENTICO_PROJECT_ID | Kentico project ID |


